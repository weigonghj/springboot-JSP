package com.czandlh.controller;

import com.czandlh.entity.admin;
import com.czandlh.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;

@Controller
public class Login {

    @Autowired
    LoginService loginService;

    @RequestMapping("/")
    public String init(){
        return "Login";
    }

    @RequestMapping("/login")
    public ModelAndView login(@PathParam("username") String username, @PathParam("password") String password, HttpSession session){
        admin admin1 = new admin();
        admin1.setUsername(username);
        admin1.setPassword(password);
        return loginService.login(admin1,session);
    }
}
