package com.czandlh.controller;

import com.czandlh.entity.users;
import com.czandlh.mapper.UsersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class User {

    @Autowired
    UsersMapper usersMapper;

    @RequestMapping("main")
    public ModelAndView main(HttpSession session){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("name",session.getAttribute("name"));
        modelAndView.addObject("users",usersMapper.selectAll());
        modelAndView.setViewName("Index");
        return modelAndView;
    }

    @RequestMapping("/user/update")
    public String update(users user){
        usersMapper.update(user);
        return "redirect:/main";
    }

    @RequestMapping("/user/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        usersMapper.deleteById(id);
        return "redirect:/main";
    }

    @RequestMapping("/user/add")
    public String add(users user){
        usersMapper.add(user);
        return "redirect:/main";
    }

}
