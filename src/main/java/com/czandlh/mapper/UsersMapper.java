package com.czandlh.mapper;

import com.czandlh.entity.users;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UsersMapper {

    List<users> selectAll();

    void update(users user);

    void deleteById(Integer id);

    void add(users user);
}
