package com.czandlh;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.czandlh.mapper")
public class SpringbootJSPApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJSPApplication.class, args);
    }

}
