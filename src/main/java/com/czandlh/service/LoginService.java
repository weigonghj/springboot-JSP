package com.czandlh.service;

import com.czandlh.entity.admin;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

public interface LoginService {
    ModelAndView login(admin admin, HttpSession session);
}
