package com.czandlh.service.impl;

import com.czandlh.entity.admin;
import com.czandlh.mapper.LoginMapper;
import com.czandlh.mapper.UsersMapper;
import com.czandlh.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    LoginMapper loginMapper;
    @Autowired
    UsersMapper usersMapper;

    @Override
    public ModelAndView login(admin admin, HttpSession session) {
        List<String> list = loginMapper.pwdByName(admin.getUsername());
        ModelAndView modelAndView = new ModelAndView();

        if (list.size()==0){
            modelAndView.addObject("error","账号错误");
            modelAndView.setViewName("Login");
            return modelAndView;
        }else {
            if (list.get(0).equals(admin.getPassword())){
                modelAndView.addObject("name",admin.getUsername());
                modelAndView.addObject("users",usersMapper.selectAll());
                modelAndView.setViewName("Index");
                session.setAttribute("name",admin.getUsername());
                return modelAndView;
            }else {
                modelAndView.addObject("error","密码错误");
                modelAndView.setViewName("Login");
                return modelAndView;
            }
        }
    }

}
