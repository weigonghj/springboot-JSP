
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>首页</title>
</head>
<body>
<h2>${name},欢迎回来...</h2>

<table>
    <tr>
        <th>ID</th>
        <th>姓名</th>
    </tr>
    <a href="Add.jsp">添加</a>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td><a href="Update.jsp?id=${user.id}&name=${user.name}">修改</a></td>
            <td><a href="/user/delete/${user.id}">删除</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>

