
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改页面</title>
</head>
<body>
    <form action="/user/update">
        <table>
            <tr>
                <th>ID</th>
                <th>姓名</th>
            </tr>
            <tr><input type="text" name="id" value="${param.id}" readonly="readonly"></tr>
            <tr><input type="text" name="name" value="${param.name}"></tr>
            <input type="submit" value="修改">
        </table>
    </form>
</body>
</html>
